package id.web.twoh.mymapapp;

import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by asus on 29/11/2016.
 */


public class pencarianjalur {

    private ListKlass lk=new ListKlass();
    public ArrayList<String> namaurutankelas=new ArrayList<>();//array untuk memanggil nama list kelas secara berurutan
    private ArrayList<Double> menghitungluasyangdilewati=new ArrayList<>();//array untuk menghitung luas list yang dilewati
    public pencarianjalur(int indexawal,int indextujuan){

        listjalur(lk.carikelas[indexawal]+" ke ",0, indexawal, indextujuan, -1, 1, caritetangga(indexawal),-1);//inputan titik awal dan tujuan/akhir

        for(String ter:namaurutankelas)
            System.out.println(ter);
    }
    private int[] caritetangga(int index){
        int[] listTetangga=new int[4];
        listTetangga[0]=lk.tdepan[index];//index untuk mencari tetangga depan
        listTetangga[1]=lk.tblakang[index];//index untuk mencari tetangga belakang
        listTetangga[2]=lk.tkanan[index];//index untuk mencari tetangga kanan
        listTetangga[3]=lk.tkiri[index];//index untuk mencari tetangga kiri

        return listTetangga;
    }
    private int[] listjalur(String untuknyimpanhasilpencarianlist,double lebare,int awal,int akhir,int posisiSkr,int idkelassinglawas,int[] gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr,int idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)//syarat untuk mencari list jalur menjadi sebuah rute
    {

        int[] listmantanTetanggaeidkelassinglawas;
        listmantanTetanggaeidkelassinglawas = new  int[3];

        while(true) {
            if(posisiSkr==-1){
                listmantanTetanggaeidkelassinglawas=caritetangga(awal);

            }else{
                listmantanTetanggaeidkelassinglawas=caritetangga(idkelassinglawas);
            }
            if(ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 0,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)){
                int tmp=posisiSkr;

                if(tmp==-1){

                    untuknyimpanhasilpencarianlist+=" , "+lk.cariposisi[listmantanTetanggaeidkelassinglawas[0]];

                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[0]];

                    tmp=0;
                }else{

                    untuknyimpanhasilpencarianlist+=" => "+lk.cariposisi[listmantanTetanggaeidkelassinglawas[0]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[0]];


                }
                if(untuknyimpanhasilpencarianlist.length()>=300)
                    break;
                if(//awal==listmantanTetanggaeidkelassinglawas[0]||
                        akhir==listmantanTetanggaeidkelassinglawas[0]){
//
                    tabuserch(untuknyimpanhasilpencarianlist, lebare);

                    break;
                }
                listjalur(untuknyimpanhasilpencarianlist,lebare, awal, akhir, tmp, listmantanTetanggaeidkelassinglawas[0], listmantanTetanggaeidkelassinglawas,idkelassinglawas);

            }
//
            if(ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 1,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)){
                int tmp=namaurutankelas.size();

                if(tmp==0){
                    untuknyimpanhasilpencarianlist+=lk.cariposisi[listmantanTetanggaeidkelassinglawas[1]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[1]];

                }else{

                    untuknyimpanhasilpencarianlist+=" => "+lk.cariposisi[listmantanTetanggaeidkelassinglawas[1]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[1]];
                }

                if(untuknyimpanhasilpencarianlist.length()>=300)
                    break;
                if(
                    //awal==listmantanTetanggaeidkelassinglawas[1]||
                        akhir==listmantanTetanggaeidkelassinglawas[1]){
//
                    tabuserch(untuknyimpanhasilpencarianlist, lebare);

                    break;

                }
                listjalur(untuknyimpanhasilpencarianlist,lebare, awal, akhir, tmp, listmantanTetanggaeidkelassinglawas[1], listmantanTetanggaeidkelassinglawas,idkelassinglawas);

            }
            if(ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 2,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)){
                int tmp=namaurutankelas.size();
                if(tmp==0){

                    untuknyimpanhasilpencarianlist+=lk.cariposisi[listmantanTetanggaeidkelassinglawas[2]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[2]];


                }else{

                    untuknyimpanhasilpencarianlist+=" => "+lk.cariposisi[listmantanTetanggaeidkelassinglawas[2]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[2]];


                }

                if(untuknyimpanhasilpencarianlist.length()>=300)
                    break;
                if(
                    //awal==listmantanTetanggaeidkelassinglawas[2]||
                        akhir==listmantanTetanggaeidkelassinglawas[2]){
//
                    tabuserch(untuknyimpanhasilpencarianlist, lebare);

                    break;
                }
                listjalur(untuknyimpanhasilpencarianlist,lebare, awal, akhir, tmp, listmantanTetanggaeidkelassinglawas[2], listmantanTetanggaeidkelassinglawas,idkelassinglawas);

            }
            if(ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 3,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)){
                int tmp=namaurutankelas.size();
                if(tmp==0){

                    untuknyimpanhasilpencarianlist+=lk.cariposisi[listmantanTetanggaeidkelassinglawas[3]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[3]];


                }else{

                    untuknyimpanhasilpencarianlist+=" => "+lk.cariposisi[listmantanTetanggaeidkelassinglawas[3]];
                    lebare+=lk.lebar[listmantanTetanggaeidkelassinglawas[3]];


                }

                if(untuknyimpanhasilpencarianlist.length()>=300)
                    break;
                if(
                    //awal==listmantanTetanggaeidkelassinglawas[3]||
                        akhir==listmantanTetanggaeidkelassinglawas[3]){
//
//
                    tabuserch(untuknyimpanhasilpencarianlist, lebare);

                    break;
                }
                listjalur(untuknyimpanhasilpencarianlist,lebare, awal, akhir, tmp, listmantanTetanggaeidkelassinglawas[3], listmantanTetanggaeidkelassinglawas,idkelassinglawas);

            }
            if(!ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 3,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)||!ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 2,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)||!ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 1,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)||!ujicobavalidgaketetanggaelekonokgakditampilno(listmantanTetanggaeidkelassinglawas, gowotonggoeuntuknyimpanhasilpencarianlistterusdileboknonangposisiSkr, 0,posisiSkr,idkelassinglawasgawengujionokgaketonggoeuntuknyimpanhasilpencarianlist)){

                break;
            }
        }
        return listmantanTetanggaeidkelassinglawas;
    }

    private boolean tabuserch(String untuknyimpanhasilpencarianlist,double lebare){
        boolean podokarobreak=false;
        if(menghitungluasyangdilewati.size()<2){
            namaurutankelas.add(untuknyimpanhasilpencarianlist);
            menghitungluasyangdilewati.add(lebare);
            // System.out.println(untuknyimpanhasilpencarianlist);
            //System.out.println("===========================================");
            podokarobreak=true;
        }
        for(int i=0;i<menghitungluasyangdilewati.size();i++)
            if(menghitungluasyangdilewati.get(i)>=lebare){

                namaurutankelas.set(i,untuknyimpanhasilpencarianlist);
                menghitungluasyangdilewati.set(i,lebare);
                //System.out.println(untuknyimpanhasilpencarianlist);
                //System.out.println("===========================================");
                break;
            }
        return podokarobreak;
    }
    private boolean ujicobavalidgaketetanggaelekonokgakditampilno(int[] tetanggaawal,int[] tenggasebelum,int posisi,int prosesmundur1kaliloncatdariposisterakhir,int prosesmundur2kaliloncatdariposisterakhir){
        boolean podokarobreak1=true;
        if(tetanggaawal[posisi]==-1){
            podokarobreak1=false;

        }


        if(prosesmundur1kaliloncatdariposisterakhir!=-1){

            if(prosesmundur2kaliloncatdariposisterakhir==tetanggaawal[posisi])
                podokarobreak1=false;
            if((tetanggaawal[posisi]==tenggasebelum[0])){
                podokarobreak1=false;

            }

            if((tetanggaawal[posisi]==tenggasebelum[1])){
                podokarobreak1=false;

            }
            if((tetanggaawal[posisi]==tenggasebelum[2])){
                podokarobreak1=false;

            }
            if((tetanggaawal[posisi]==tenggasebelum[3])){
                podokarobreak1=false;
            }
        }

        return podokarobreak1;
    }

}

