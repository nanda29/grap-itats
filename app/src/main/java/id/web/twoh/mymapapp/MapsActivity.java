package id.web.twoh.mymapapp;

/**
 * Created by nanda on 31/10/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsoluteLayout;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import java.util.Arrays;
import java.util.List;


//import com.google.android.gms.maps.SupportMapFragment;

public class MapsActivity extends AppCompatActivity implements View.OnClickListener

{

    ListKlass lk=new ListKlass();

    pencarianjalur pj;


    private boolean tamsearch = false;
    private WindowManager tampilane;
    private boolean searchActive = false;
    private MenuItem searchItem;
    private MenuItem rute;
    private Toolbar toolbare;
    ImageView cleartext;
    ImageView cleartext1;
    private AutoCompleteTextView actv;
    private AutoCompleteTextView actv1;
    private SearchView mSearchView;
    private ImageView tiwal;
    RelativeLayout zoomm;
    RelativeLayout rutene;
    ZoomControls simpleZoomControls;
    ImageButton butar1;
    ImageButton butar2;
    ImageButton butar3;
    ImageButton butar4;
    ImageButton balekawal;
    private GestureDetector mGestureDetector;
    private float scale = 1f;
    private ScaleGestureDetector detector;
    int pos;
    int car;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //menampilkan map
        setContentView(R.layout.activity_maps);




        //zoom
        zoomm = (RelativeLayout) findViewById(R.id.maps);// initiate a ImageView


        //arraymencariposisiawal
        //Membuat  ArrayAdapter dari string nama language
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, lk.cariposisi);
        //Mendapatkan instansi dari AutoCompleteTextView
        actv = (AutoCompleteTextView) findViewById(R.id.alcarpos);
        actv.setThreshold(1);//Akan bekerja saat karakter pertama
        actv.setAdapter(adapter);//Mensetting data adapter ke dalam AutoCompleteTextView
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actv.getWindowToken(), 0);
                String selected = (String) parent.getItemAtPosition(position);
                pos = Arrays.asList(lk.cariposisi).indexOf(selected);
                OnSearch(lk.w[pos], lk.h[pos], lk.x[pos], lk.y[pos], lk.cariposisi[pos]);


            }
        });


        //arraymencarikelas
        //Membuat  ArrayAdapter dari string nama language
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, lk.carikelas);
        //Mendapatkan instansi dari AutoCompleteTextView
        actv1 = (AutoCompleteTextView) findViewById(R.id.alcarkel);
        actv1.setThreshold(1);//Akan bekerja saat karakter pertama
        actv1.setAdapter(adapter1);//Mensetting data adapter ke dalam AutoCompleteTextView
        actv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm1 = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm1.hideSoftInputFromWindow(actv1.getWindowToken(), 0);
                String selected1 = (String) parent.getItemAtPosition(position);
                car = Arrays.asList(lk.carikelas).indexOf(selected1);
                Onkelas(lk.w1[car], lk.h1[car], lk.x1[car], lk.y1[car], lk.carikelas[car]);

                if(!actv.getText().equals("Posisi Anda")&&!actv1.getText().equals("Kelas Tujuan")){
            }


            }
        });


        simpleZoomControls = (ZoomControls) findViewById(R.id.simpleZoomControl); // initiate a ZoomControls
        simpleZoomControls.hide();// initially hide ZoomControls from the screen
        // perform setOnTouchListener event on ImageVie
        zoomm.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // show Zoom Controls on touch of image
                simpleZoomControls.show();
                return false;
            }
        });
        // perform setOnZoomInClickListener event on ZoomControls
        simpleZoomControls.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calculate current scale x and y value of ImageView
                    float x = zoomm.getScaleX();
                    float y = zoomm.getScaleY();
                    zoomm.setScaleX((float) (x + 0.15));
                    zoomm.setScaleY((float) (y + 0.15));

                    // display a toast to show Zoom In Message on Screen
                // hide the ZoomControls from the screen
                simpleZoomControls.hide();

            }
        });
        // perform setOnZoomOutClickListener event on ZoomControls
        simpleZoomControls.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RelativeLayout bat=(RelativeLayout)findViewById(R.id.utama);
                // calculate current scale x and y value of ImageView
                float x = zoomm.getScaleX();
                float y = zoomm.getScaleY();
                // set decreased value of scale x and y to perform zoom out functionality
//                if(x<450&&y<665) {
//                    zoomm.setScaleX((float) (x));
//                    zoomm.setScaleY((float) (y));
//                }else
                    zoomm.setScaleX((float) (x - 0.15));
                    zoomm.setScaleY((float) (y - 0.15));



                // display a toast to show Zoom Out Message on Screen
                // hide the ZoomControls from the screen
                simpleZoomControls.hide();

            }

        });


//arrow
        arrow();


//

        tiwal = (ImageView) findViewById(R.id.posan);
        mSearchView = new SearchView(this);


//        tiwal=(ImageView) findViewById(R.id.tiwal);
        if (toolbare != null){
            toolbare.post(new Runnable() {
                @Override
                public void run() {
                    if (!tamsearch && tampilane != null) {
//                        tampilane.addView(tamsearch,
//                                SearchView.LayoutParams(MapsActivity));
//                        tamsearch = true;
                    }
                }
            });
        }


        cleartext =(ImageView)findViewById(R.id.cleartext);
        cleartext.setOnClickListener(this);
        cleartext1 =(ImageView)findViewById(R.id.cleartext1);
        cleartext1.setOnClickListener(this);
        }


    //posisiawal
    public void OnSearch (int ww , int hh , int xx,int yy, String nama){

        lk.rlParent =(RelativeLayout)findViewById(R.id.rlParent);
        EditText location_tf = (EditText)findViewById(R.id.alcarpos);
        String location= location_tf.getText().toString();
        AbsoluteLayout.LayoutParams aa=new AbsoluteLayout.LayoutParams( ww,hh,xx,yy);
        if(lk.rlParent==null){

            lk.rlParent.setVisibility(View.INVISIBLE);
        }else
            lk.rlParent.setVisibility(View.VISIBLE);
            lk.rlParent.setLayoutParams(aa);
    }

    //kelastujuan
    public void Onkelas (int ww , int hh , int xx,int yy, String nama) {
        lk.rlParent1 = (RelativeLayout) findViewById(R.id.rlParent1);

        EditText location_tf = (EditText) findViewById(R.id.alcarkel);
        String location = location_tf.getText().toString();
        AbsoluteLayout.LayoutParams aa = new AbsoluteLayout.LayoutParams(ww, hh, xx, yy);
        if (lk.rlParent1 == null) {

            lk.rlParent1.setVisibility(View.INVISIBLE);
        }else
            lk.rlParent1.setVisibility(View.VISIBLE);
            lk.rlParent1.setLayoutParams(aa);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        searchItem = menu.findItem(R.id.rute);
        //rute = menu.findItem(R.id.menuq);

        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item==searchItem) {
                    Intent on=new Intent(MapsActivity.this, rutte.class);
                    on.putExtra("spos",pos);
                    on.putExtra("scar",car);
                    startActivity(on);
                }
                return true;
            }
        });

        if(searchActive)
            startActivity(new Intent(this, MapsActivity.class));



        return true;
    }

    public void onClick(View v ) {
        if (v == cleartext) {
            actv.setText("");

        } else if (v == cleartext1) {
            actv1.setText("");

        }
    }




public void arrow(){
    zoomm = (RelativeLayout) findViewById(R.id.maps);
    butar1 = (ImageButton) findViewById(R.id.kanan);// initiate a ZoomControls
    butar2 = (ImageButton) findViewById(R.id.kiri);// initially hide ZoomControls from the screen
    butar3 = (ImageButton) findViewById(R.id.atas);// initiate a ZoomControls
    butar4 = (ImageButton) findViewById(R.id.bawah);// initially hide ZoomControls from the screen

    // perform setOnTouchListener event on ImageVie


    butar1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // calculate current scale x and y value of ImageView
            float x = zoomm.getScrollX();
            float y = zoomm.getScrollY();
            // set decreased value of scale x and y to perform zoom out functionality
//                if(zoomm>x&&zoomm>y){
            zoomm.setScrollX((int)  (x+10));
            zoomm.setScrollY((int) (y));

            // display a toast to show Zoom Out Message on Screen
            // hide the ZoomControls from the screen


        }
    });
    butar2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // calculate current scale x and y value of ImageView
            int x = zoomm.getScrollX();
            int y = zoomm.getScrollY();
            // set decreased value of scale x and y to perform zoom out functionality
//                if(zoomm>x&&zoomm>y){
            zoomm.setScrollX((int) (x-10));
            zoomm.setScrollY((int) (y));

            // display a toast to show Zoom Out Message on Screen
            // hide the ZoomControls from the screen


        }
    });
    butar3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // calculate current scale x and y value of ImageView
            float x = zoomm.getScrollX();
            float y = zoomm.getScrollY();
            // set decreased value of scale x and y to perform zoom out functionality
//                if(zoomm>x&&zoomm>y){
            zoomm.setScrollX((int)  (x));
            zoomm.setScrollY((int) (y-10));

            // display a toast to show Zoom Out Message on Screen
            // hide the ZoomControls from the screen


        }
    });
    butar4.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // calculate current scale x and y value of ImageView
            int x = zoomm.getScrollX();
            int y = zoomm.getScrollY();
            // set decreased value of scale x and y to perform zoom out functionality
//                if(zoomm>x&&zoomm>y){
            zoomm.setScrollX((int) (x));
            zoomm.setScrollY((int) (y+10));

            // display a toast to show Zoom Out Message on Screen
            // hide the ZoomControls from the screen


        }
    });



}
}
